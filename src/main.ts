import * as THREE from "three"
import { BoingScene } from "./scenes/BoingScene"

const clackSound = new Audio("clack.mp3")
let hasSoundEnabled = false

const canvas = document.getElementById("viewport") as HTMLCanvasElement

const renderer = new THREE.WebGLRenderer({ canvas })
renderer.setPixelRatio(window.devicePixelRatio)

const camera = new THREE.PerspectiveCamera(50, 1, 0.1, 1000)
camera.position.z = 250.0

const initViewport = () => {
  camera.aspect = window.innerWidth / window.innerHeight
  camera.updateProjectionMatrix()
  renderer.setSize(window.innerWidth, window.innerHeight)
}

initViewport()
window.addEventListener("resize", initViewport)

const handleBounce = () => {
  if (hasSoundEnabled) {
    clackSound.play()
  }
}

const scene = new BoingScene({
  onBounce: handleBounce,
})

document.getElementById("toggle-axes")?.addEventListener("click", () => {
  scene.hasVisibleAxes = !scene.hasVisibleAxes
})

document.getElementById("toggle-sound")?.addEventListener("click", () => {
  hasSoundEnabled = !hasSoundEnabled
})

const render = () => {
  scene.computeFrame()
  renderer.render(scene, camera)
  requestAnimationFrame(render)
}

render()
