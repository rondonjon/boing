import { Color, Mesh, MeshBasicMaterial, SphereGeometry } from "three"

export class Shadow extends Mesh {
  constructor(radius: number, widthSegments = 16, heightSegments = 8) {
    super()
    this.geometry = new SphereGeometry(radius, widthSegments, heightSegments) // Model space is -1...+1
    this.material = new MeshBasicMaterial({
      color: new Color(0, 0, 0),
      transparent: true,
      opacity: 0.5,
    })
  }
}
