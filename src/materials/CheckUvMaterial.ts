import { Color, ShaderMaterial } from "three"

/**
 * Material which renders a checkboard pattern based on the u/v texture coordinates
 * which are expected to span the entire geometry within a single 0/0 to 1/1 range.
 * The number of horizontal and vertical boxes is determined by the `widthSegments`
 * and `heightSegments` parameters.
 */
export class CheckUvMaterial extends ShaderMaterial {
  constructor(widthSegments: number, heightSegments: number, color1: number, color2: number) {
    super({
      uniforms: {
        fWidthSegments: {
          value: widthSegments,
        },
        fHeightSegments: {
          value: heightSegments,
        },
        vColorEven: {
          value: new Color(color1),
        },
        vColorOdd: {
          value: new Color(color2),
        },
      },
      vertexShader: `
          varying vec2 vUv;
          void main() {
            vUv = uv;
            gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
          }`,
      fragmentShader: `
          uniform float fHeightSegments;
          uniform float fWidthSegments;
          uniform vec3 vColorEven;
          uniform vec3 vColorOdd;
          varying vec2 vUv;
          void main() {
            // uv in range 0..1 -> scale up to 0..segments -> remainder %2 -> determine if even or odd tile
            bool bFillX = mod(vUv.x * fWidthSegments, 2.0) >= 1.0;
            bool bFillY = mod(vUv.y * fHeightSegments, 2.0) >= 1.0;
            // combine x/y fill states with XOR (^^) for checkboard effect
            bool bFillXY = bFillX ^^ bFillY;
            // set colors
            gl_FragColor = vec4(bFillXY ? vColorEven : vColorOdd, 1.0);
          }`,
    })
  }
}
