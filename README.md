# boing! in ThreeJS

A ThreeJS/WebGL port of the famous "Bouncing Ball" demo that was originally made on the Amiga
to demonstrate its capabilities and later copied on [many other systems](https://www.youtube.com/watch?v=fSwwqt3ue2M).

Made in 2022 as an exercise after a years-long break from 3D, WebGL, and shader programming.

Latest deployed version: https://rondonjon.gitlab.io/boing

<img src="docs/screenshot.png" alt="screenshot" width="640" height="355" />

## My takeaways

### This is some really old stuff!

I stumbled on specifications dating back to as far as 2013 which is already looking very extensive
and still relatively up to date. Surprised that the 3D/web topic hasn't gained more momentum in
almost a decade.

### Nothing is easy

The idea of refreshing my memory with a "funny" and "easy" task by recreating the "Boing" demo
from the 1980s turned out more difficult than I had expected.

After browsing the ThreeJS documentation, I started off with a `SphereGeometry` for the ball,
but soon after, the troubles began, and the hardest part were the materials.

I didn't follow the first impulse to use a bitmap texture because it seemed so utterly wrong to use
bitmaps, and I assumed that the checkboard pattern wouldn't come out sharp and crisp if I did.
I wanted to identify the faces in the sphere geometry and assign colors to them as I vaguely remembered
doing it in the past, but I found no way to even access the faces and address them in a logical order
in tenable time. So finally I went with shaders and the U/V coordinates that ThreeJS thankfully spans
in a convenient 0/0 to 1/1 range spanned accross the entire sphere.

I had obviously repressed the memory of how difficult it is to get started with shader programming,
without a verbose debugger running along and funny unexpected results such as those caused by
precision loss in floating point computations.

### `react-three-fiber`?

Coming from React Frontend development and having missed out on the ThreeJS/WebGL proceedings of the
past years, I stumbled upon [Sohail's videos](https://www.youtube.com/watch?v=y5CAuAZ7kKM) and started
into this demo with `react-three-fiber`, a package that allows us to write ThreeJS code declaratively
inside React components.

After some time I decided to scrap this approach because I really couldn't see the benefit. The
declarative notation differs strongly from ThreeJS in the past, and having to translate every snippet
of sample code only to try it out seemed too cumbersome.

### Recap: vertex and fragment shaders

Fragment shaders render surfaces, pixel by pixel. As surfaces are indirectly defined through
vertices, we typically need some information on the vertices that define this surface, such
as their position, an eventually assigned color, or texture coordinates.

To access such information in a fragment shader, we must export that information from a
corresponding vertex shader, which means that we typically need to write two shaders and not
just a fragment shader.

The other reason why we might want to implement a vertex shader is that they allow us to
manipulate the geometry of the scene at render time and create all kinds of stunning effects
such as [ocean waves](https://threejs.org/examples/?q=water#webgl_shaders_ocean) even on
surfaces that are actually flat. But to keep it simple, let us assume we only want to pass
data to the fragment shader.

Shaders work with values of different visibilties: a `uniform` is constant in the entire
render process, a `varying` is shared between vertex and fragment shaders, and local
variables are only visible for the current function call.

One very essential but not so obvious problem with vertex shaders is that they **must** return
a vertex position in `gl_Position`, else the rendered object will simply disappear from the
screen when the shader is added. The computation should include **all** transformations from
model space to viewport coordinates, which makes this a non-trivial task.

Apparently, there are solutions around this problem, like the `EffectComposer`, which allows
us to render in [multiple passes](https://www.airtightinteractive.com/2013/02/intro-to-pixel-shaders-in-three-js/),
or the recently added `Material.onBeforeCompile` callback, which would allow us to
[base our shader on some other existing shader](https://threejs.org/docs/index.html#api/en/materials/Material.onBeforeCompile),
but since this demo doesn't rely on any of the "default" features such lighting, I didn't
(have to) try it out.

Luckily, there is a third solution which is requires only a one-liner to define `gl_Position`
because ThreeJS thankfully [defines](https://threejs.org/docs/#api/en/renderers/webgl/WebGLProgram)
all the transformation matrices that we need.

In the following example, we copy another built-in value, the `uv` texture coordinates of
the vertex, to a variable so that the fragment shader can read it:

```c
varying vec2 vUv;
void main() {
  vUv = uv;
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
```

Important to understand about fragment shaders is that they receive the values from vertex
shaders **pre-interpolated**. So if we are rendering a point of a triangle, the exported
values of the three vertices that make up this triangle will be merged before they are sent
to the fragment shader, and that in proportion to their respective proximity to the point
that we are rendering.

### The `render()` loop

Unsurprisingly, the use of `requestAnimationFrame()` resulted in the smoothest possible animations:

```ts
const render = () => {
  // ...
  renderer.render(scene, camera)
  requestAnimationFrame(render)
}
render()
```

This renders as many frames as possible on the system's hardware.

### Frame Computation

Many examples that I found used the `render()` function not only to update the display, but also to update
the scene logic in static increments (e.g. add a certain value to the rotation angle in every frame).
This seemed unfavourable as it would result in different animation speeds on different systems, depending on
their render performance. Moving the frame computation to an interval however resulted in unround animations.
At the end, I achieved the best results by keeping the computation in the `render()` function, but by deriving
all state from the system time:

```ts
public computeFrame() {
  const now = new Date().getTime()
  const frame = (now - this.initTimestamp) / 10 // derive other states from this virtual "frame" counter
  // ...
}
```

## Helpful resources

The following resources turned out as very helpful:

- [Pixel Shaders in Three.js](https://www.airtightinteractive.com/2013/02/intro-to-pixel-shaders-in-three-js/), an intro to shader programming in ThreeJS with a great coverage of the `EffectComposer`
- [WebGL-shaders.com](https://webgl-shaders.com/about.html) with many examples and a good link collection
- [ThreeJS.org examples](https://threejs.org/examples) with even more examples (with source code accessible through the links in the lower-right corner of each demo)
- [The Book of Shaders](https://thebookofshaders.com/), an intro to shader programming in general
- [OpenGL GL4 Reference](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/indexflat.php) as reference for shader programming
- [WebGLProgram](https://threejs.org/docs/#api/en/renderers/webgl/WebGLProgram) as a reference of built-in uniforms and attributes available in ThreeJS shaders
- [Sodaphonic](https://sodaphonic.com/editor), an online sound recorder and editor

Other interesting findings:

- [Imitating StandardMaterial lights in ShaderMaterial](https://discourse.threejs.org/t/imitating-standardmaterial-lights-in-shadermaterial/12136) and [Extending three.js materials with GLSL](https://medium.com/@pailhead011/extending-three-js-materials-with-glsl-78ea7bbb9270) on how to merge shaders in ThreeJS
- [https://threejs.org/examples/?q=webgl_postprocessing_fxaa#webgl_postprocessing_fxaa](https://threejs.org/examples/?q=webgl_postprocessing_fxaa#webgl_postprocessing_fxaa) as an FXAA example (anti-aliasing in a post-processing step)

## License

Copyright © 2022 "Ron Don Jon" Daniel Kastenholz.

Released under the [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).

## History

| Version | Changes                                         |
| ------- | ----------------------------------------------- |
| 1.0.0   | Initial version                                 |
| 1.1.0   | Added clack sound, split header/footer          |
| 1.1.1   | Fix asset location in deployments               |
| 1.2.0   | Use (mobile-friendlier) buttons in footer       |
| 1.3.0   | Remove floor, use Atari / fabric shader on wall |
| 1.4.0   | Add shadow                                      |
| 1.5.0   | Cleanup dependencies, add Hushy precommit hook  |
| 1.6.0   | Add homepage / repository links to package.json |
